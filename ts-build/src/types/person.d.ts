export declare enum Role {
    ADMIN = 5,
    READ_ONLY = 6,
    AUTHOR = 7
}
export declare const person: {
    name: string;
    age: number;
    hobbies: string[];
    role: [number, string];
    role2: Role;
    anyField: any;
};
