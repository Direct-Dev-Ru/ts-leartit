declare type Combinable = number | string;
declare type ConversionDescriptor = 'as-number' | 'as-string';
export declare function add(n1: number, n2: number, showResult: boolean, phrase: string): number;
export declare function combine(input1: Combinable, input2: Combinable, showResult?: boolean, phrase?: string, conversion?: ConversionDescriptor): Combinable;
export declare let combineValues: (n1: Combinable, n2: Combinable, b: boolean, p: string, c: ConversionDescriptor) => Combinable;
export declare function multAdd(...numbers: number[]): number;
export {};
