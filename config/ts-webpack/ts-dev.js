const paths = require('../paths');

const webpack = require('webpack');
const {merge} = require('webpack-merge');

const common = require('./ts-common');

module.exports = merge(common, {
  mode: 'development',
  devtool: 'eval-cheap-source-map',

  devServer: {
    static: {
      directory: paths.build,
    },
    historyApiFallback: true,
    liveReload: true,
    compress: true,
    port: 9000,
    open: true,
    hot: true,
  },

  plugins: [new webpack.HotModuleReplacementPlugin()],
});
