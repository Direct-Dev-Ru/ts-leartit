const paths = require('../paths');

const webpack = require('webpack');

const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const Dotenv = require('dotenv-webpack');

const babelLoader = {
  loader: 'babel-loader',
  options: {
    presets: ['@babel/preset-env', '@babel/preset-react'],
    plugins: [
      '@babel/plugin-proposal-class-properties',
      '@babel/plugin-syntax-dynamic-import',
      '@babel/plugin-transform-runtime',
    ],
  },
};

module.exports = {
  target: ['web', 'es2015'],
  entry: `${paths.src}/index.ts`,
  output: {
    clean: true,
    path: paths.tsbuild,
    filename: 'js/[name].bundle.js',
    publicPath: '/',
    crossOriginLoading: 'anonymous',
    module: true,
    environment: {
      arrowFunction: true,
      bigIntLiteral: false,
      const: true,
      destructuring: true,
      dynamicImport: false,
      forOf: true,
    },
  },
  //   devtool: 'inline-source-map',
  resolve: {
    alias: {
      '@modules': `${paths.src}/modules`,
      '@functions': `${paths.src}/functions`,
      '@types': `${paths.src}/types`,
    },
    extensions: ['.tsx', '.ts', '.js', '.jsx'],
  },
  experiments: {
    topLevelAwait: true,
    outputModule: true,
  },
  module: {
    rules: [
      // JavaScript, React
      {
        test: /\.m?jsx?$/i,
        exclude: /node_modules/,
        use: babelLoader,
      },
      // TypeScript
      {
        test: /.tsx?$/i,
        exclude: /node_modules/,
        use: [babelLoader, 'ts-loader'],
      },
      // CSS, SASS
      {
        test: /\.(c|sa|sc)ss$/i,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {importLoaders: 1},
          },
          'sass-loader',
        ],
      },
      // MD
      {
        test: /\.md$/i,
        use: ['html-loader', 'markdown-loader'],
      },
      // static files
      {
        test: /\.(jpe?g|png|gif|svg|eot|ttf|woff2?)$/i,
        type: 'asset',
      },
    ],
  },
  plugins: [
    new webpack.ProgressPlugin(),

    new CopyWebpackPlugin({
      patterns: [
        {
          from: `${paths.public}/assets`,
          to: './assets',
        },
        {
          from: `${paths.public}/styles`,
          to: './styles',
        },
      ],
    }),

    new HtmlWebpackPlugin({
      template: `${paths.public}/index.html`,
      filename: 'index.html',
      title: 'Webpack Course',
      minify: {
        collapseWhitespace: true,
        removeComments: true,
        removeRedundantAttributes: true,
        useShortDoctype: true,
      },
      templateParameters: {
        analytics: 'Google Analytics ID',
        author: 'Direct-Dev.Ru',
        publishedDate: '2021-12-27',
        description:
          'Webpack 5 Boilerplate for JavaScript, React & TypeScript projects',
        keywords:
          'webpack, webpack5, boilerplate, template, max, config, bundler, bundle, javascript, react, reactjs, react.js, typescript, project, app',
        title: 'Webpack Course !!!',
        url: 'https://example.com',
      },
    }),

    new webpack.ProvidePlugin({
      React: 'react',
    }),

    new Dotenv({
      path: './config/.env',
    }),
  ],
};
