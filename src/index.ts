/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable prefer-const */

import './scss/style.scss';

import {someDomElement} from './types/type-casting';

console.log(someDomElement);

import {add, combine, combineValues, multAdd} from './functions/adding';
import {person} from './types/person';
import {
  Department,
  ITDepartment,
  AccountingDepartment,
} from './classes/Department';
import {Employee} from './classes/Person';

import {Counter} from './classes/Singleton(Counter)';

import {
  Subject,
  iPerson as iPerson,
  iGreetable as iGreetable,
} from './interfaces/first-interface';

console.log('---------Interface section----------------');
const person1: iGreetable = new Subject('anton', 43);
const subject1: Subject = <Subject>person1;
subject1.name = 'antonio';
subject1.describe();
person1.greet('Hello');
console.log('---------End of Interface section----------');

const counter = Counter.getInstance(0);
counter.increment(2);
console.log('counter.counterValue: ', counter.counterValue);

// const itDept = new Department('1', 'Accounting');
const itDept = new ITDepartment('1', ['Accounting']);
// console.log('Class object of Department:', itDept);
itDept.describe();
itDept.addEmployee(new Employee('Anton', 43, 'it spec', 40_000));
itDept.addEmployee(new Employee('Elena', 42, 'accounter', 25_000));
itDept.addEmployee(new Employee('Kate', 13, 'schoolgirl', 0));
itDept.prindEmployee();

const accountingDept = new AccountingDepartment('33', [
  'Year Balance',
  'Nalogovaya Declaraciya',
  'Billing Report',
]);

accountingDept.mostRecentReport = 'Otchet o pribilah i ubitkah';
accountingDept.addEmployee('Maxim');

accountingDept.print();

// const accountingCopy = {
//   name: 'DUMMY',
//   describe: itDept.describe,
//   describeWitoutThis: itDept.describeWitoutThis,
// };
// accountingCopy.describe();

const accountingCopy = {
  describeWitoutThis: itDept.describeWitoutThis.bind(itDept),
};
accountingCopy.describeWitoutThis();

let phrase = 'Result of function add is: ';
const docDOM = window.document;
const number1 = 5.1;
let number2 = 7.7;
const showInConsole = true;
// showInConsole = false;
number2 *= 2;
// docDOM.getElementById('headerDiv').innerText = `${phrase} ${add(
//   number1,
//   number2,
//   showInConsole,
//   phrase
// )}`;

const heading = document.createElement('h1');
heading.textContent = 'Header Div!';
docDOM.getElementById('headerDiv').append(heading);

const headerParagraph = document.createElement('p');

phrase = 'Result of function combine as-number is: ';

headerParagraph.textContent = `${phrase} ${combine(
  number1,
  number2,
  showInConsole,
  phrase,
  'as-number'
)}`;
docDOM.getElementById('headerDiv').append(headerParagraph);

const bodyHeading = document.createElement('h1');
bodyHeading.textContent = 'Body Div!';
docDOM.getElementById('bodyDiv').append(bodyHeading);

phrase = 'Result of function combine as-string is: ';

const bodyParagraph = document.createElement('p');

bodyParagraph.textContent = `${phrase} ${combineValues(
  'Anton',
  'Kuznetcov',
  showInConsole,
  phrase,
  'as-string'
)}`;
docDOM.getElementById('bodyDiv').append(bodyParagraph);

class Game {
  name = 'Violin Charades';
}
const myGame = new Game();

// создаем параграф
const gameParagraph = document.createElement('p');
gameParagraph.textContent = `I like ${myGame.name}.`;

// создаем элемент заголовка
const gameHeading = document.createElement('h3');
gameHeading.textContent = 'What game do I like very much ???';

// добавляем параграф и заголовок в DOM
docDOM.getElementById('bodyDiv').append(gameHeading, gameParagraph);

const hobbies = ['Sports', 'Cooking'];
const activeHobbies = ['Hiking', ...hobbies];
// console.log(activeHobbies);

const arrayOfNumbers = [3, 4, 5 / 6, 7.8, 9, 22, 33];
// console.log(multAdd(...arrayOfNumbers));
