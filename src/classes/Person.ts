abstract class Person {
  static getPersonId(name: string, prefix = 'person') {
    if (!name) {
      throw new Error('No name provided for employee id creation');
    }
    /**
     * TODO: Make saving to permanent store generated ids
     */
    return `${prefix}-${Math.round(Math.random() * 10_000)}`;
  }
  constructor(private readonly id: string) {}

  public name: string;
  public age: number;

  abstract describe(this: Person): void;
}

export class Employee extends Person {
  public profession: string;
  protected salary: number;
  constructor(name: string, age: number, profession: string, salary: number) {
    super(Employee.getPersonId(name, 'employee'));
    this.name = name;
    this.age = age;
    this.profession = profession;
    this.salary = salary;
  }

  public get salaryValue(): number {
    return this.salary;
  }

  describe(this: Employee): void {
    console.log(
      `This is the employee with name:${this.name} age:${this.age} and profession is ${this.profession}`
    );
  }
}
