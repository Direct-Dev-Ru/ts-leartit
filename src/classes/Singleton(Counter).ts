// type AddFn = (a: number, b: number) => number;

// Functional type in interfaces
interface AddFn {
  (a: number, b: number): number;
}
const add: AddFn = (a: number, b: number) => a + b;

export class Counter {
  private static instance: Counter;
  private counter: number;

  private constructor(private initialCountValue: number = 0) {
    const lastCounter = window.localStorage.getItem('lastCounter');
    this.counter =
      lastCounter && !isNaN(+lastCounter) ? +lastCounter : initialCountValue;
  }

  static getInstance(initialCounterValue = 0) {
    if (Counter.instance) {
      return this.instance;
    }
    this.instance = new Counter(initialCounterValue);
    return this.instance;
  }

  public get counterValue(): number {
    return this.counter;
  }

  public set counterValue(v: number) {
    this.counter = v;
    window.localStorage.setItem('lastCounter', v.toString());
  }

  public increment(delta = 1) {
    this.counterValue = add(this.counterValue, delta);
  }

  public decrement(delta = 1) {
    this.counterValue -= delta;
  }
}
