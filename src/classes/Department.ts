import {Employee} from './Person';
/**
 *
 * @param id
 * @param name
 * ! Comment this is comment
 * * Important info
 * ? The question is asked to myself
 * TODO: HBZ to write
 */
export class Department {
  static fiscalYear = 2021;
  //   public name: string;
  //   private readonly id: string;
  protected employees: Employee[] = [];

  constructor(private readonly id: string, public name: string) {
    // this.id = id;
    // this.name = n;
  }
  // dummy parameter for type safety
  describe(this: Department) {
    console.log(`Department:${this.name}`);
  }

  describeWitoutThis() {
    console.log('Department without this:', this.name);
  }

  addEmployee(employee: Employee | string) {
    if (typeof employee === 'string') {
      return;
    }
    this.employees.push(employee);
  }

  prindEmployee() {
    console.log('Employee Length: ', this.employees.length);
    console.log('Employee Content: ', this.employees);
  }

  //   static
  static createEmployee(name: string) {
    if (!name) {
      throw new Error('No name provided for employee creation');
    }
    return new Employee(name, 33, 'car driver', 23_000);
  }
}

export class ITDepartment extends Department {
  admins: string[];
  constructor(id: string, admins: string[]) {
    super(id, 'IT');
    this.admins = admins;
  }
}

export class AccountingDepartment extends Department {
  private lastReport: string;
  constructor(id: string, private reports: string[]) {
    super(id, 'Accounting');
    this.lastReport = reports[0];
  }

  get mostRecentReport() {
    if (this.lastReport) {
      return this.lastReport;
    }
    throw new Error('No last report');
  }

  set mostRecentReport(value: string) {
    if (!value) {
      throw new Error('No value provided');
    }
    this.addReport(value);
    this.lastReport = value;
  }

  addEmployee(employee: string): void {
    if (employee === 'Anton') {
      return;
    }
    // shouid be protected not private in base class
    this.employees.push(Department.createEmployee(employee));
    // super.addEmployee(
    //   new Person(`employee-${Math.round(Math.random() * 100)}`, employee)
    // );
  }

  addReport(text: string) {
    this.reports.push(text);
    this.lastReport = text;
  }

  print(this: AccountingDepartment): void {
    console.log('[This is accounting department object:]', this);
  }

  printReports(): void {
    console.log('[This is accounting department object:]', this.reports);
  }
}
