interface iNamed {
  name: string;

  //   optional
  displayName?: string;
}

export interface iPerson extends iNamed {
  readonly age: number;

  describe(this: iPerson): void;
}

export interface iGreetable {
  name: string;
  greet(phrase: string): void;
}

export class Subject implements iGreetable, iPerson {
  displayName: string;
  constructor(public name: string, public readonly age: number) {
    this.displayName = this.name.charAt(0).toUpperCase() + this.name.slice(1);
  }

  greet(phrase: string) {
    console.log(`${phrase} ${this.displayName ? this.displayName : this.name}`);
  }
  describe() {
    console.log(
      `${this.displayName ? this.displayName : this.name} is age of ${this.age}`
    );
  }
}

// export const user1: Person = {
//   name: 'Anton',
//   age: 43,
//   describe(this: Person) {
//     console.log(`${this.name} is age of ${this.age}`);
//   },
//   greet(phrase: string) {
//     console.log(`${phrase} ${this.name}`);
//   },
// };
