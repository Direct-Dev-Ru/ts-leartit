/* eslint-disable @typescript-eslint/no-unused-vars */
// types can be intersected by two ways

// type Admin = {
interface Admin {
  name: string;
  privileges: string[];
}

// type Employee = {
interface Employee {
  name: string;
  startDate: Date;
}

// type ElevatedEmployee = Admin & Employee;
interface ElevatedEmployee extends Admin, Employee {}

const elEmployee: ElevatedEmployee = {
  name: 'Anton',
  privileges: ['create-database', 'create-server'],
  startDate: new Date(),
};
console.log('[elEmployee]:', elEmployee);
// types guarding

type Combinable = number | string;
type Numeric = number | boolean;

type Universal = Combinable & Numeric;

function addCombinableVars(a: Combinable, b: Combinable) {
  let numA: number;
  let numB: number;

  if (typeof a === 'string' || typeof b === 'string') {
    numA = a;
    numB = b;
  } else {
    numA = +a;
    numB = +b;
  }
  return numA + numB;
}

type UnknownEmployee = Employee | Admin;
function printEmpInf(emp: UnknownEmployee) {
  console.log(emp.name);
  if ('privileges' in emp) {
    console.log(`Privileges: ${emp.privileges}`);
  }
  if ('startDate' in emp) {
    console.log(`StartDate is ${emp.startDate}`);
  }
}

class Car {
  constructor(public vendor: string) {}
  drive() {
    console.log('Driving ...');
  }
}

class Truck {
  constructor(public vendor: string) {}
  drive() {
    console.log('Driving a truck ...');
  }

  loadCargo(amount: number) {
    console.log(`Loading cargo ... ${amount}`);
  }
}
type Vehicle = Car | Truck;

const v1 = new Car('Niva');
const v2 = new Truck('Kamaz');

function useVehicle(vehicle: Vehicle) {
  vehicle.drive();
  //   if ('loadCargo' in vehicle){
  //       vehicle.loadCargo(10_000);
  //   }
  if (vehicle instanceof Truck) {
    vehicle.loadCargo(10_000);
  }
}

useVehicle(v1);
useVehicle(v2);
