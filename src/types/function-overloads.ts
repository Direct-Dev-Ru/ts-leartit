/* eslint-disable @typescript-eslint/no-unused-vars */
type CombinableClone = number | string;
type NumericClone = number;

type UniversalClone = CombinableClone & NumericClone;

function addCombinables(a: number, b: number): number;
function addCombinables(a: string, b: number): string;
function addCombinables(a: number, b: string): string;
function addCombinables(a: string, b: string): string;
function addCombinables(
  a: CombinableClone,
  b: CombinableClone
): CombinableClone {
  if (
    typeof a === 'string' &&
    typeof b === 'string' &&
    (isNaN(+a) || isNaN(+b))
  ) {
    return `${a}${b}`;
  }

  return +a + +b;
}

const result = addCombinables(4, 3);

console.log(result);
