// const someDomElement = <HTMLInputElement>document.getElementById('message-in');

// if we sure it is not null
// export const someDomElement = document.getElementById(
//   'message-in'
// )! as HTMLInputElement;

// or if it can be null
export const someDomElement = document.getElementById('message-in');
if (someDomElement) {
  (someDomElement as HTMLInputElement).value = 'Hi there!';
}

// index types
interface ErrorContainer {
  [prop: string]: string;
}

export const errorBag: ErrorContainer = {
  email: 'Not a valid email!',
  username: 'Must start with a capital character!',
};
