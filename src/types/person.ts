/* eslint-disable prefer-const */
/* eslint-disable @typescript-eslint/no-explicit-any */
export enum Role {
  ADMIN = 5,
  READ_ONLY,
  AUTHOR,
}

export const person: {
  name: string;
  age: number;
  hobbies: string[];
  role: [number, string]; //Tuple
  role2: Role; //Enum
  anyField: any;
} = {
  name: 'Anton',
  age: 43,
  hobbies: ['Sport', 'Cooking'],
  role: [2, 'author'],
  role2: Role.ADMIN,
  anyField: new Date(),
};

let hobbies: string[];
hobbies = ['Eating', 'Sleeping'];

person.hobbies = hobbies;

person.role = [0, 'admin'];
for (const hobby of person.hobbies) {
  console.log(hobby.toUpperCase());
}
