/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable prefer-const */

type Combinable = number | string;
type ConversionDescriptor = 'as-number' | 'as-string';

export function add(
  n1: number,
  n2: number,
  showResult: boolean,
  phrase: string
): number {
  const result = n1 + n2;
  if (showResult) {
    console.log(`${phrase} ${result}`);
  }
  return result;
}

export function combine(
  input1: Combinable,
  input2: Combinable,
  showResult = true,
  phrase = 'The result is: ',
  conversion: ConversionDescriptor = 'as-number'
): Combinable {
  let result;
  if (
    (typeof input1 === 'number' && typeof input2 === 'number') ||
    conversion === 'as-number'
  ) {
    result = +input1 + +input2;
  } else {
    result = input1.toString() + ' ' + input2.toString();
  }
  if (showResult) {
    console.log(`${phrase} ${result}`);
  }
  return result;
}

export let combineValues: (
  n1: Combinable,
  n2: Combinable,
  b: boolean,
  p: string,
  c: ConversionDescriptor
) => Combinable;

combineValues = combine;

function addAndHandle(n1: number, n2: number, cb: (num: number) => void) {
  const result = n1 + n2;
  const cbResult = cb(result);
  console.log(cbResult);
}
addAndHandle(10, 20, (n: number) => {
  console.log(n);
  return n;
});

let userInput: unknown;
let userName: string;

userInput = 'dd';
// userName = userInput; //Type 'unknown' is not assignable to type 'string'.ts(2322)
if (typeof userInput === 'string') {
  userName = userInput;
}

// never type

function generateError(message: string, code: number): never {
  throw {message, errorCode: code};
}
// generateError('An Error occur', 500);

// rest parameters
export function multAdd(...numbers: number[]) {
  let result = 0;
  for (const iterator of numbers) {
    result += iterator;
  }
  return result;
}
